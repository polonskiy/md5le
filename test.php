<?php

require __DIR__.'/md5le.php';

$secret = sha1(uniqid(mt_rand(), 1));

$data = 'original data'.mt_rand();
$sign = md5($secret.$data);
assert(md5($secret.$data) === $sign);

$evil_data = 'evil data'.mt_rand();
$keylen = strlen($secret); //we neew to know secret length (or bruteforce it)
$datalen = strlen($data);

$data_chunk = pad($data, $datalen + $keylen);
$pad = substr($data_chunk, $datalen);

$evil_chunk = pad($evil_data, strlen($data_chunk) + strlen($evil_data) + $keylen);
$evil_sign = _md5le($evil_chunk, $sign);
$hdata = $data.$pad.$evil_data;
assert(md5($secret.$hdata) === $evil_sign);
